<?php

namespace WPDesk\Migrations\Tests\Finder;

use WPDesk\Migrations\Finder\GlobFinder;
use PHPUnit\Framework\TestCase;

class GlobFinderTest extends TestCase {

	public function test_skip_unexpected_classes() {
		$finder = new GlobFinder();
		$migrations = $finder->find_migrations(__DIR__ . '/../../../../fixtures/migrations');

		self::assertCount(2, $migrations);
		self::assertContains(
			'WPDesk\Migrations\Tests\fixtures\migrations\Version_10',
			$migrations
		);

		self::assertContains(
			'WPDesk\Migrations\Tests\fixtures\migrations\Version_11',
			$migrations
		);

		self::assertNotContains(
			'WPDesk\Migrations\Tests\fixtures\migrations\Borked_Version_01',
			$migrations
		);
	}

}
