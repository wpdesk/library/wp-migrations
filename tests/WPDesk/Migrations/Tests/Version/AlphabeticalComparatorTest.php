<?php

namespace WPDesk\Migrations\Tests\Version;

use PHPUnit\Framework\TestCase;
use WPDesk\Migrations\Version\AlphabeticalComparator;
use WPDesk\Migrations\Version\Version;

class AlphabeticalComparatorTest extends TestCase {

	public function test_10_is_less_than_11() {
		$comparator = new AlphabeticalComparator();

		$result = $comparator->compare(
			new Version('Version_10'),
			new Version('Version_11')
		);

		self::assertTrue($result === -1);

		$result = $comparator->compare(
			new Version('Version_11'),
			new Version('Version_10')
		);

		self::assertTrue($result === 1);

		$result = $comparator->compare(
			new Version('Version_10'),
			new Version('Version_10')
		);

		self::assertTrue($result === 0);
	}
}
