<?php

namespace WPDesk\Migrations\Tests;

use Psr\Log\NullLogger;
use WPDesk\Migrations\FilesystemMigrationsRepository;
use PHPUnit\Framework\TestCase;
use WPDesk\Migrations\Finder\GlobFinder;
use WPDesk\Migrations\Version\AlphabeticalComparator;
use WPDesk\Migrations\Version\WpdbMigrationFactory;

class FilesystemMigrationsRepositoryTest extends TestCase {

	public function test_migrations_sorted() {
		$migration_directories = [
			__DIR__ . '/../../../fixtures/migrations'
		];
		$migrations_repository = new FilesystemMigrationsRepository(
			$migration_directories,
			new GlobFinder(),
			new WpdbMigrationFactory(
				new \wpdb(),
				new NullLogger()
			),
			new AlphabeticalComparator()
		);

		$sorted_migrations = $migrations_repository->get_migrations();
		self::assertEquals(
			(string) $sorted_migrations[0]->get_version(),
			'WPDesk\Migrations\Tests\fixtures\migrations\Version_10'
		);
		self::assertEquals(
			(string) $sorted_migrations[1]->get_version(),
			'WPDesk\Migrations\Tests\fixtures\migrations\Version_11'
		);
	}

}
