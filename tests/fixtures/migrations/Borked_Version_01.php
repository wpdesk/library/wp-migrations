<?php
declare( strict_types=1 );

namespace WPDesk\Migrations\Tests\fixtures\migrations;

use WPDesk\Migrations\AbstractMigration;

/** Invalid name */
class Borked_Version_01 extends AbstractMigration {

	public function up(): bool {
		return true;
	}
}
