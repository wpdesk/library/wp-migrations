<?php
declare(strict_types=1);

namespace WPDesk\Migrations\Finder;

use WPDesk\Migrations\AbstractMigration;

interface MigrationFinder {

	/**
	 * @param string $directory
	 * @return class-string<AbstractMigration>[]
	 */
	public function find_migrations( string $directory ): array;
}
