<?php
declare(strict_types=1);

namespace WPDesk\Migrations;

interface Migrator {

	public function migrate(): void;
}
