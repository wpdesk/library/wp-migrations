# WP Migrations

Simple library based on `doctrine/migrations` for managing database
versioned schema.

## Installation

`composer require wpdesk/wp-migrations`

## Usage

To operate on WordPress database you need to create class extending
`WPDesk\Migrations\AbstractMigration`.

```php
$migrator = WpdbMigrator::from_classes(
  [Version_01012022::class, Version_12012022::class],
  'unique_db_option_name'
);
$migrator->migrate();
```

With multiple migration sources it may be inconvenient to type each class in array. In such case it may be better to group all migrations in directories. In such case it is required that (besides each class MUST extend `WPDesk\Migrations\AbstractMigration` class) file names starting with **Version** prefix.

Assuming following directory structure:

```
project\
  migrations\
    Version_01012022.php
    Version_12012022.php
    XVersion_13012022.php # This will be skipped
  main.php
```

You can register your migration classes in the following way:

```php
$migrator = WpdbMigrator::from_directories(
  [__DIR__ . '/migrations'],
  'unique_db_option_name'
);
$migrator->migrate();
```
