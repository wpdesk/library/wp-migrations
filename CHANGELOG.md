# WP Migrations Changelog

## [1.1.0] - 12-11-2024
### Added
- New method to migration classes: `is_needed()` which can return false to skip specific migration version execution.
### Removed
- Removed `AbstractMigration::down()` as in WordPress context it is impossible to execute down migrations reliably and the method was never actually used.

## [1.0.4] - 06-10-2023
### Fixed
- Respect status returned from migration. If any error occurred (migration returned false or threw), break migration process.

## [1.0.3] - 19-12-2022
### Fixed
- Comparing migrations correctly installs newer version on fresh instance.

## [1.0.2] - 12-12-2022
### Fixed
- Migration no longer iterates over all registered entries. Install only the newest versions.

## [1.0.1] - 15-06-2022
### Fixed
- Improved saving log entries to database.

## [1.0.0] - 14-06-2022
### Added
- Initial release.
